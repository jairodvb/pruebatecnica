// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAQ-tLdTTegaa6Xga1m2WljnIk1u5GnOU0",
    authDomain: "pruebatecnica-a47b0.firebaseapp.com",
    databaseURL: "https://pruebatecnica-a47b0.firebaseio.com",
    projectId: "pruebatecnica-a47b0",
    storageBucket: "",
    messagingSenderId: "374209661599",
    appId: "1:374209661599:web:58ddc61a419889a9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
