import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewListComponent } from './components/view-list/view-list.component';
import { CreateItemComponent } from './components/create-item/create-item.component';
import { ViewItemsComponent } from './components/view-items/view-items.component';
import { DeleteItemComponent } from './components/delete-item/delete-item.component';
import { EditItemComponent } from './components/edit-item/edit-item.component';


const routes: Routes = [
  {path: 'viewlist', component: ViewListComponent},
  {path: 'createitem', component: CreateItemComponent},
  {path: 'edititem', component: EditItemComponent},
  {path: 'deleteitem', component: DeleteItemComponent},
  {path: 'viewitems', component: ViewItemsComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
