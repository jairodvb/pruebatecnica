import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { item } from 'src/app/models/items.class';
import { DatosService } from 'src/app/services/datos.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-delete-item',
  templateUrl: './delete-item.component.html',
  styleUrls: ['./delete-item.component.css']
})
export class DeleteItemComponent implements OnInit {

 

  columnDefs = [
    { headerName: 'Id', field: 'id', filter: true, width: 300 },
    { headerName: 'Título', field: 'title', filter: true, width: 300 }];

  defaultColDef = { resizable: true, filter: true };
  rowData = [];
  private gridApi;
  private gridColumnApi;
  public rowSelection;


  constructor(public datosService: DatosService, private _Spinner: NgxSpinnerService, private _alert: AlertService) {
    this.rowSelection = 'single';
  }

  ngOnInit() {
    this.obtenerItems();
  }

  obtenerItems() {
    this.datosService.getItems().pipe(
      finalize(() => {
        this._Spinner.hide();
      })
    ).subscribe(data => {
      this.rowData = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as item;
      })
    });

  }

  onSelectionChanged(e: any) {
    var selectedRows = this.gridApi.getSelectedRows();
    var selectedRowsString = '';
    selectedRows.forEach(function(selectedRow, index) {
      if (index !== 0) {
        selectedRowsString += ', ';
      }
      selectedRowsString += selectedRow;
      console.log(selectedRow);
    });
    
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

}
