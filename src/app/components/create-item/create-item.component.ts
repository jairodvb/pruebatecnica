import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DatosService } from 'src/app/services/datos.service';
import { finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { item } from 'src/app/models/items.class';
import { AlertService } from 'ngx-alerts';



@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.scss']
})
export class CreateItemComponent implements OnInit {

  items: FormGroup;
  idItem: number;
  mensaje: any;
  itemSave: item;

  constructor(public datos:DatosService, private router: Router, private _Spinner: NgxSpinnerService,
    private _alert: AlertService) { }

  ngOnInit() {
    this.items = new FormGroup({
      id: new FormControl(''),
      title: new FormControl('')
    });
  }

  onSubmit(){
    
    this._Spinner.show();
    this.datos.CreateItem(this.items.value).then(
      res => {
        this.resetFields();
        this.router.navigate(['/viewitems']);
        this._alert.success('Se ha almacenado con éxito');
        this.mensaje = res;
        console.log(this.mensaje);
        
      }
    )
  }

  resetFields(){
    this.items = new FormGroup({
      id: new FormControl(''),
      title: new FormControl('')
    });
  }

}
