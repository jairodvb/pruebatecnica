import { Component, OnInit } from '@angular/core';
import { DatosService } from 'src/app/services/datos.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'ngx-alerts';
import { finalize, retry } from 'rxjs/operators';
import { item } from 'src/app/models/items.class';

@Component({
  selector: 'app-view-items',
  templateUrl: './view-items.component.html',
  styleUrls: ['./view-items.component.scss']
})
export class ViewItemsComponent implements OnInit {

  columnDefs = [
    { headerName: 'Id', field: 'id', filter: true, width: 300 },
    { headerName: 'Título', field: 'title', filter: true, width: 300 }];

  defaultColDef = { resizable: true, filter: true };
  rowData = [];

  constructor(public datosService: DatosService, private _Spinner: NgxSpinnerService, private _alert: AlertService) { }

  ngOnInit() {
    this.obtenerItems();
  }

  obtenerItems(){
    this.datosService.getItems().pipe(
      finalize(() => {
        this._Spinner.hide();
      })
    ).subscribe(data => {
      this.rowData = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as item;
      })
    });
   
  }

}
