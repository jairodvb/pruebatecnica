import { Component, OnInit } from '@angular/core';
import { DatosService } from 'src/app/services/datos.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'ngx-alerts';
import { finalize, retry } from 'rxjs/operators';


@Component({
  selector: 'app-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.scss']
})
export class ViewListComponent implements OnInit {

  columnDefs = [
    {headerName: 'Id', field: 'id', filter: true, width: 300 },
    {headerName: 'Título', field: 'title', filter: true, width: 300 }];

defaultColDef = { resizable: true, filter: true };


rowData = [];


  constructor(public datosService: DatosService, private _Spinner: NgxSpinnerService, private _alert: AlertService) { }

  ngOnInit() {
    this.obtenerDatos();
  }

  obtenerDatos() {
    this._Spinner.show();
    this.datosService.GetInformacion().pipe(
      finalize(() => {
        this._Spinner.hide();
      })
    ).subscribe(
      (data: any) => {
        this.rowData = data;
      }
    );
  }



}
