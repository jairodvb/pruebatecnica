import { Component, OnInit } from '@angular/core';
import { DatosService } from 'src/app/services/datos.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'ngx-alerts';
import { item } from 'src/app/models/items.class';
import { finalize } from 'rxjs/operators';


@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {

  columnDefs = [
    { headerName: 'Id', field: 'id', filter: true, width: 300 },
    { headerName: 'Título', field: 'title', filter: true, width: 300 }];

  defaultColDef = { resizable: true, filter: true };
  rowData = [];
  private gridApi;
  private gridColumnApi;
  public rowSelection;
  public display;


  constructor(public datosService: DatosService, private _Spinner: NgxSpinnerService, private _alert: AlertService) {
    this.rowSelection = 'single';
    this.display = 'none';

  }

  ngOnInit() {
    this.obtenerItems();
  }

  obtenerItems() {
    this.datosService.getItems().pipe(
      finalize(() => {
        this._Spinner.hide();
      })
    ).subscribe(data => {
      this.rowData = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as item;
      })
    });

  }

  onSelectionChanged(e: any) {
    var selectedRows = this.gridApi.getSelectedRows();
    var selectedRowsString = '';
    selectedRows.forEach(function(selectedRow, index) {
      if (index !== 0) {
        selectedRowsString += ', ';
      }
      selectedRowsString += selectedRow;
      console.log(selectedRow);
    });
    
    
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  mostrarModal(){

  }

}
