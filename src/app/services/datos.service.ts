import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseService } from './basic.service';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class DatosService extends BaseService {

  PathApi = 'https://jsonplaceholder.typicode.com/todos';

  constructor(private http: HttpClient, public db: AngularFirestore) { 
    super();
  }

  GetInformacion(){
    return this.http.get(this.PathApi);
  }

  CreateItem(item: any){
    return this.db.collection('itemsdb').add({
      id: item.id,
      title: item.title,
      idunico: new Date().getTime()
    });
  }

  getItems(){
     return this.db.collection('itemsdb').snapshotChanges();
  }

  updateItem(userKey, value: any){
    return this.db.collection('itemsdb').doc(userKey).set(value);
  }

  deleteUser(userKey){
    return this.db.collection('users').doc(userKey).delete();
  }


}
